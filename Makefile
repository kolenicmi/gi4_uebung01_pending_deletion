CC = gcc
CFLAGS = -Wall

.PHONY: all default clean

default: myecho myenv isset exec fork

all: myecho myenv isset exec fork

myecho: myecho.c

myenv: myenv.c

isset: isset.c

exec: exec.c

fork: fork.c

test: myecho myenv isset exec fork
	./myecho
	./myenv
	./isset
	./exec
	./fork

clean:
	rm -f myecho myenv isset exec fork
